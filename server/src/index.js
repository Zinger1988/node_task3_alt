const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const errorHandler = require('./services/errorHandler');
const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const app = express();
dotenv.config();

mongoose.connect(process.env.DB_CONN)
  .then(() => app.listen(process.env.PORT))
  .catch((err) => console.error(`Error on server startup: ${err.message}`));

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);
app.use(errorHandler);
