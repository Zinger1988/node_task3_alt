const mongoose = require('mongoose');

const { Schema } = mongoose;
const { LOAD_STATUSES, LOAD_STATES } = require('../constatnts');

const schema = new Schema(
  {
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    status: {
      type: String,
      enum: Object.values(LOAD_STATUSES),
      default: LOAD_STATUSES.NEW,
      required: true,
    },
    state: {
      type: String,
      enum: Object.values(LOAD_STATES),
      default: LOAD_STATES.UNSET,
    },
    name: {
      type: String,
      required: true,
    },
    payload: {
      type: Number,
      required: true,
    },
    pickup_address: {
      type: String,
      required: true,
    },
    delivery_address: {
      type: String,
      required: true,
    },
    dimensions: {
      width: {
        type: Number,
        required: true,
      },
      length: {
        type: Number,
        required: true,
      },
      height: {
        type: Number,
        required: true,
      },
    },
    logs: [
      {
        message: {
          type: String,
          default: 'Unset',
        },
        time: {
          type: String,
          default: new Date().toISOString(),
        },
      },
    ],
    created_date: {
      type: String,
      required: true,
      default: new Date().toISOString(),
    },
  },
);

const Load = mongoose.model('Load', schema);

module.exports = Load;
