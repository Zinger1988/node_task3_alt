const bcryptjs = require('bcryptjs');
const User = require('../models/userModel');
const Truck = require('../models/truckModel');
const Load = require('../models/loadModel');
const throwError = require('../services/throwError');
const { ROLES, TRUCK_STATUSES } = require('../constatnts');

const getUserInfo = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findById(userId);

  if (!user) {
    throwError(400, 'e01');
  }

  res.json({ user });
};

const deleteUser = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findById(userId);

  if (!user) {
    throwError(400, 'e01');
  }

  if (user.role === ROLES.DRIVER) {
    const isTruckOnLoad = await Truck.findOne({ created_by: user._id, status: TRUCK_STATUSES.ON_LOAD });

    if (isTruckOnLoad) {
      throwError(400, 'e05');
    }

    await Truck.deleteMany({ created_by: user._id });
  }

  if (user.role === ROLES.SHIPPER) {
    await Load.deleteMany({ created_by: user._id });
  }

  await user.deleteOne();
  res.status(200).json({ message: 'User was deleted' });
};

const changeUserPassword = async (req, res) => {
  const { userId } = req.user;
  const user = await User.findById(userId);
  const { oldPassword, newPassword } = req.body;

  if (!user) {
    throwError(400, 'e01');
  }

  if (await bcryptjs.compare(String(oldPassword), String(user.password))) {
    await user.updateOne({
      password: await bcryptjs.hash(newPassword, 10),
      $push: {
        logs: {
          message: 'The user\'s password has been changed',
          time: new Date().toISOString(),
        },
      },
    });
    res.status(200).json({ message: 'Password was successfully changed' });
  } else {
    throwError(400, 'e08');
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
