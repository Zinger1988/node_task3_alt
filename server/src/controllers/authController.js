const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const User = require('../models/userModel');
const mailer = require('../services/nodemailer');
const throwError = require('../services/throwError');

dotenv.config();

const signUp = async (req, res) => {
  const { email, password, role } = req.body;
  const newUser = new User({ email, role, password: await bcryptjs.hash(password, 10) });

  await newUser.save();
  res.status(200).json({ message: 'Profile created successfully' });
};

const signIn = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) {
    throwError(400, 'e06');
  }

  if (await bcryptjs.compare(String(password), String(user.password))) {
    const payload = {
      email: user.email,
      userId: user._id,
    };
    const jwt_token = jwt.sign(payload, process.env.TOKEN_SECRET);
    res.status(200).json({ jwt_token });
  } else {
    throwError(400, 'e06');
  }
};

const passwordRecovery = async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });

  if (user) {
    const randomPassword = Math.random().toString(36).slice(-8);
    await user.updateOne({
      password: await bcryptjs.hash(randomPassword, 10),
      $push: {
        logs: {
          message: 'The user\'s password has been recovered',
          time: new Date().toISOString(),
        },
      },
    });

    const message = {
      to: email,
      subject: 'Password recovery',
      text: `Your new password is ${randomPassword}`,
    };

    mailer(message);
    res.status(200).json({ message: 'New password was sent to your email address' });
  } else {
    throwError(400, 'e06');
  }
};

module.exports = {
  signUp,
  signIn,
  passwordRecovery,
};
