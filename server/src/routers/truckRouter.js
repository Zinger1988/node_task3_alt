const express = require('express');

const router = express.Router();
const asyncWrapper = require('../services/asyncWrapper');
const checkToken = require('../middleware/checkToken');
const { truckValidation } = require('../middleware/validationTruck');

const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruckToDriver,
} = require('../controllers/truckController');

router.get('/', checkToken, asyncWrapper(getTrucks));
router.post('/', checkToken, truckValidation, asyncWrapper(addTruck));
router.get('/:id', checkToken, asyncWrapper(getTruck));
router.put('/:id', checkToken, truckValidation, asyncWrapper(updateTruck));
router.delete('/:id', checkToken, asyncWrapper(deleteTruck));
router.post('/:id/assign', checkToken, asyncWrapper(assignTruckToDriver));

module.exports = router;
