const express = require('express');

const router = express.Router();
const asyncWrapper = require('../services/asyncWrapper');

const {
  signUpValidation,
  loginValidation,
  passwordRecoveryValidation,
} = require('../middleware/validationAuth');

const {
  signUp,
  signIn,
  passwordRecovery,
} = require('../controllers/authController');

router.post('/register', signUpValidation, asyncWrapper(signUp));
router.post('/login', loginValidation, asyncWrapper(signIn));
router.post('/forgot_password', passwordRecoveryValidation, asyncWrapper(passwordRecovery));

module.exports = router;
