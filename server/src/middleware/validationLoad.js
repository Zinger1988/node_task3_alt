const Joi = require('joi');

const addLoadValidation = async (req, res, next) => {
  try {
    const schema = Joi.object({
      name: Joi
        .string()
        .required(),
      payload: Joi
        .number()
        .required(),
      pickup_address: Joi
        .string()
        .required(),
      delivery_address: Joi
        .string()
        .required(),
      dimensions: Joi
        .object({
          width: Joi
            .number()
            .required(),
          length: Joi
            .number()
            .required(),
          height: Joi
            .number()
            .required(),
        })
        .required(),
    });

    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

module.exports = {
  addLoadValidation,
};
